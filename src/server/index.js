const http = require("http");
const path = require("path");
const fs = require("fs");
const uuid4 = require("uuid4");

const readFilePromise = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, filename), "utf-8", (err, data) => {
      if (err) {
        reject(err.message);
      } else {
        resolve(data);
      }
    });
  });
};

const server = http.createServer((request, response) => {
  const url = request.url.toString().split("/");
  const modifiedUrl = "/" + url[1];

  switch (modifiedUrl) {
    case "/":
      readFilePromise("../public/index.html")
        .then((data) => {
          response.writeHead(200, { "content-type": "text/html" });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          console.error(err);
        });
      break;

    case "/style.css":
      readFilePromise("../public/style.css")
        .then((data) => {
          response.writeHead(200, { "content-type": "text/css" });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          console.error(err);
        });
      break;

    case "/html":
      readFilePromise("../public/info.html")
        .then((data) => {
          response.writeHead(200, { "content-type": "text/html" });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          console.error(err);
        });
      break;

    case "/json":
      readFilePromise("../data/data.json")
        .then((data) => {
          response.writeHead(200, { "content-type": "application/json" });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          console.error(err);
        });
      break;

    case "/uuid":
      const uuid = uuid4();

      response.writeHead(200, { "content-type": "application/json" });
      response.write(
        JSON.stringify({
          uuid: uuid,
        })
      );
      response.end();
      break;

    case "/status":
      const urlArr = request.url.toString().split("/");
      const statusCode = Number(urlArr[urlArr.length - 1]);

      if (statusCode < 200) {
        response.end("Status code: " + statusCode);
      } else {
        response.writeHead(statusCode, { "content-type": "text/plain" });
        response.write("Status code: " + statusCode);
        response.end();
      }
      break;

    case "/delay":
      const getUrl = request.url.toString().split("/");
      const delay = Number(getUrl[getUrl.length - 1]);
      const delayMillisecond = delay * 1000;

      if (delay > 20) {
        response.writeHead(200, { "content-type": "text/plain" });
        response.write("Delay must be less than 20 sec.");
        response.end();
      } else {
        setTimeout(() => {
          response.writeHead(200, { "content-type": "text/plain" });
          response.write("Successfully responded after " + delay + " second.");
          response.end();
        }, delayMillisecond);
      }
      break;

    default:
      response.writeHead(404, { "content-type": "text/plain" });
      response.write("404 Page Not Found");
      response.end();
  }
});

const port = 3000;

server.listen(port, () => {
  console.log("Server Listening on Port " + port);
});
